
#include <Timestamp.h>

#include <Wire.h>
#include <DS3231.h>

#include <SD.h>
#include <Adafruit_BME280.h>


//
#define FAST_LED 8
#define SLOW_LED 9

DS3231* rtc;
Adafruit_BME280 bme; 

volatile byte speed_factor = 100;
volatile byte speed_count = 0;


File record;
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Wire.begin();
  init_broadcast(16, 17);
  randomSeed(analogRead(0));
  rtc = new DS3231();
  byte time[7] = {0xFF,0,0,0,0,0,0};
  Serial.print("update DS3231 time by the following format (binary): 0xEE, HH, MM, SS, DD, MM, YY");
  Serial.readBytes(time, 7);
  if(time[0]==0xEE){
    rtc->setSecond(time[3]);
    rtc->setMinute(time[2]);
    rtc->setHour(time[1]);
    rtc->setDate(time[4]);
    rtc->setMonth(time[5]);
    rtc->setYear(time[6]);
    rtc->setClockMode(false); 
  }
  //SD init 
  pinMode(10, OUTPUT); //to avoid spi slave issue
  Serial.begin(9600);
  if(!SD.begin(10))
    Serial.print("SD init error");
  if(!SD.exists(F("/greensat"))) {
    SD.mkdir(F("/greensat"));
  }
  record = SD.open("/greenSat/record.txt", FILE_WRITE);
  while(!record) {
  }
  //set up speed control
  slow();
  //set up interrupt
  attachInterrupt(digitalPinToInterrupt(2), fast, FALLING);
  attachInterrupt(digitalPinToInterrupt(3), slow, FALLING);
  //set LED indicators
  setupLED(FAST_LED);
  setupLED(SLOW_LED);
  free(rtc);
  //set up BME
  if (!bme.begin()) {
    Serial.println(F("Could not find a valid BME280 sensor, check wiring!"));
    while (1);
  } else {
    Serial.println(F("BME ready"));
  }
  //done
  delay(100);
  Serial.print(F("start"));
}
char* buf;
byte timeStamp[6];
bool temp;
void loop() {
  // put your main code here, to run repeatedly:
  //Serial.print(F("enter"));
  //Serial.print(F("SRAM left 0: "));
  //Serial.println(freeRam());
  getTimeStamp(timeStamp);
  //Serial.print(F("SRAM left 1: "));
  //Serial.println(freeRam());
  broadcast(timeStamp, 6, 16, 17);
  buf = malloc(30*sizeof(char));
  sprintf(buf, "%02d:%02d:%02d:%02d:%02d:%02d",timeStamp[0],timeStamp[1],timeStamp[2],timeStamp[3],timeStamp[4],timeStamp[5]);
  Serial.write(buf);
  Serial.write("\n");
  if(should_execute()) {
    Serial.print(buf);
    Serial.print("T:");
    Serial.print(bme.readTemperature());
    Serial.print("P:");
    Serial.print(bme.readPressure());
    Serial.print("H:");
    Serial.print(bme.readHumidity());
    Serial.print("\n");
    record.print(buf);
    record.print("T:");
    record.print(bme.readTemperature());
    record.print("P:");
    record.print(bme.readPressure());
    record.print("H:");
    record.print(bme.readHumidity());
    record.print("\n");
    record.flush();
    Serial.print(F("write"));
  }
  free(buf);
  //Serial.print(F("SRAM left 2: "));
  //Serial.println(freeRam());
  delay(10);
}
//grab timestamp
void getTimeStamp(byte* timeStamp) {
  rtc = new DS3231();
  timeStamp[2] = (byte)rtc->getSecond();
  timeStamp[1] = (byte)rtc->getMinute();
  timeStamp[0] = (byte)rtc->getHour(temp, temp);
  timeStamp[3] = (byte)rtc->getDate();
  timeStamp[4] = (byte)rtc->getMonth(temp);
  timeStamp[5] = (byte)rtc->getYear();
  //Serial.print((byte)rtc->getYear());
  free(rtc);
}


//TEMP implementation
void init_broadcast(int B_SDA, int B_SCL) {
  //init
  pinMode(B_SDA, OUTPUT);
  pinMode(B_SCL, OUTPUT);
  digitalWrite(B_SDA, HIGH);
  digitalWrite(B_SCL, HIGH);
  delay(100);
}

//dataform: n-bytes are transmitted in I2C style (without master and ack), from MSB to LSB and no intervals between bytes
void broadcast(byte* data, int len, int B_SDA, int B_SCL) {

  //start transmission
  digitalWrite(B_SDA, LOW); //pull B_SDA from high to low while B_SCL is high
  delayMicroseconds(8); //expected transmission rate: ?hz
  //Serial.write("Start transmission\n");
  //
  byte temp = 0x00;
  byte sum = 0x00;
  const byte mask = 0x80;
  for (int byteCount = 0; byteCount <= len; byteCount++) {
    if(byteCount < len) {
      temp = data[byteCount];
      sum += temp;
    } else
      temp =  sum;
    for (int bitCount = 7; bitCount >= 0; bitCount--) { //doing my magic
      digitalWrite(B_SCL, LOW);
      delayMicroseconds(8);
      digitalWrite(B_SDA, mask & temp ? HIGH : LOW);
      delayMicroseconds(8);
      digitalWrite(B_SCL, HIGH);
      delayMicroseconds(8);
      temp <<= 1;
    }
    delayMicroseconds(8);
  }
  //end transmission
  //Serial.write("end transmission");
  delayMicroseconds(8);
  digitalWrite(B_SCL, LOW);
  delayMicroseconds(8);
  digitalWrite(B_SDA, LOW);
  delayMicroseconds(8);
  digitalWrite(B_SCL, HIGH);
  delayMicroseconds(8);
  digitalWrite(B_SDA, HIGH);
}

//speedControl
boolean should_execute() {
  speed_count++;
  if(speed_count == speed_factor)
    speed_count = 0;
  return !(speed_count % speed_factor);
}
boolean setSpeedFactor(unsigned int factor) {
  speed_factor = factor;
  speed_count = 0;
}
void slow() {
  setSpeedFactor(100);
  LED_Off(FAST_LED);
  LED_On(SLOW_LED);
}
void fast() {
  setSpeedFactor(10);
  LED_On(FAST_LED);
  LED_Off(SLOW_LED);
}
void setupLED(int pin) {
  pinMode(pin, OUTPUT);
}
void LED_On(int pin) {
  digitalWrite(pin, HIGH);
}
void LED_Off(int pin) {
  digitalWrite(pin, LOW);
}
int freeRam() {
  extern int __heap_start,*__brkval;
  int v;
  return (int)&v - (__brkval == 0 ? (int)&__heap_start : (int) __brkval);  
}